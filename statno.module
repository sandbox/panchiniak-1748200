<?php
/**
 * @file
 * This is statno.module for Drupal. Refers to statistics of taxonomy
 */

define('STATNO_VERSION', '7.x-1.x-dev');

/**
 * Implements hook_permission().
 */
function statno_permission() {
  return array(
    'administer statno' => array(
      'title' => t('Administer statno'),
      'description' => t('statno settings'),
      'restrict access' => TRUE,
    ),
  );
}
/**
 * Implements hook_help().
 *
 * Displays help and module information.
 *
 * @param string $path
 *   Which path of the site we're using to display help
 * @param string $arg
 *   Array that holds the current path as returned from arg() function
 *
 * @return string
 *   $items
 */
function statno_help($path, $arg) {
  switch ($path) {
    case "admin/help#statno":
      return '<p>' . t('Statno generates statistics for taxonomies') . '</p>';
      break;
  }
}
/**
 * Implements hook_menu().
 */
function statno_menu() {
  $items['admin/config/content/statno'] = array(
    'title' => 'statno',
    'description' => 'Settings of statno module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('statno_form'),
    'access arguments' => array('administer statno'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}
/**
 * Implements hook_form().
 *
 * Form function, called by drupal_get_form()
 * in statno_menu().
 */
function statno_form($form, &$form_state) {
  $form['statno_vocabulary'] = array(
    '#type' => 'textfield',
    '#title' => t('Vocabulary machine name'),
    '#default_value' => variable_get('statno_vocabulary', 'vocabulary'),
    '#size' => 40,
  );
  $form['statno_term'] = array(
    '#type' => 'textfield',
    '#title' => t('Term'),
    '#default_value' => variable_get('statno_term', 'type'),
    '#size' => 40,
  );
  //$form['statno_sincroniza'] = array(
  //  '#type' => 'textfield',
  //  '#title' => t('Sincroniza a estatisticas das taxonomias'),
  //  '#default_value' => variable_get('statno_sincroniza', '0'),
  //  '#size' => 40,
  //);
  return system_settings_form($form);
}
/**
 * Implements hook_block_info().
 */
function statno_block_info() {
  $blocks['statno'] = array(
    'info' => t('Índice Statno'), //The name that will appear in the block list.
    'cache' => DRUPAL_CACHE_PER_ROLE, //Default
  );
  return $blocks;
}
/**
 * Implements hook_block_view().
 * 
 * Prepares the contents of the block.
 */
function statno_block_view($delta = '') {
  switch($delta){
    case 'statno':
      $block['subject'] = t('Índice');
      if(user_access('access content')){
        ////Use our custom function to retrieve data.
        ////$result = current_posts_contents();
        //$result = statno_countterm(1);
        ////Array to contain items for the block to render.
        //$items = array();
        ////Iterate over the resultset and format as links.
        //foreach ($result as $node){
        //  $items[] = array(
        //    'data' => l($node->title, 'node/' . $node->nid),
        //  ); 
        //}
        //
        //if (empty($items)) { //No content in the last week.
        //  $block['content'] = t('No posts available.');  
        //} 
        //else {
        //  //Pass data through theme function.
        //  $block['content'] = theme('item_list', array(
        //    'items' => $items));
        //}
        //$termo_result = statno_termnames();
        //foreach ($termo_result as $termo){
        //  $block['content'] = var_dump($termo);
        //}
        $qma = statno_qma();
        $qta = statno_qta();
        if ($qma>0){
          $qtq = $qta/($qma*2);
        }
        else{
          $qtq = 0;
        }
        //$qtq = $qta/($qma*2);
        $block['content'] = t('Qma: '.$qma.' Qta: '.$qta.' Qtq: '.round($qtq).'<br>'.statno_estatistica());
        
        //$block['content'] = t('O qma é '.statno_qma());
        
        //$block['content'] = t('Vocabulário: '.variable_get('statno_vocabulary', 'vocabulary').'<br>');
        
      }
  }
  return $block;
}
/**
 * Custom statno function
 *
 * Checkes the existence of given vocabulary machine name
 *
 * @param string $vocabulary_name
 *   v.machine_name
 *
 * @return array
 *   An associative array containing:
 *     machine_name
 */
function statno_checkvocabulary($vocabulary_name) {
  $query_checkvocabulary = db_select('taxonomy_vocabulary', 'v');
  $result_checkvocabulary = $query_checkvocabulary
    ->fields('v', array('machine_name'))
    ->fields('v', array('vid'))
    ->condition('v.machine_name', $vocabulary_name, '=')
    ->execute()
    ->fetchAssoc();
  return $result_checkvocabulary;  
}
/**
 * Custom statno function
 *
 * Checkes the existence of given term
 *
 * @param string $term_name
 *   v.machine_name
 *
 * @return array
 *   An associative array containing:
 *     machine_name
 */
function statno_checkterm($term_name,$vid) {
  $query_checkterm = db_select('taxonomy_term_data', 't');
  $result_checkterm = $query_checkterm
    ->fields('t', array('name'))
    ->fields('t', array('tid'))
    ->condition('t.name', $term_name, '=')
    ->condition('t.vid', $vid, '=')
    ->execute()
    ->fetchAssoc();
  return $result_checkterm;
}
/**
 * Implements validation from the Form API.
 *
 * @param array $form
 *   A structured array containing the elements and properties of the form.
 * @param array $form_state
 *   An array that stores information about the form's current state
 *   during processing.
 */
function statno_form_validate($form, &$form_state) {
  $vocabulary_name = $form_state['values']['statno_vocabulary'];
  if (!$vocabulary_name) {
    form_set_error('statno_vocabulary', t('You should set some vocabulary.'));
  }
  elseif (!($vacabulary_data = statno_checkvocabulary($vocabulary_name))) {
    form_set_error('statno_vocabulary', t('The vocabulary machine name does not exist.'));    
  }
  $term_name = $form_state['values']['statno_term'];
  if (!$term_name) {
    form_set_error('statno_term', t('You should set some term.'));
  }
  elseif (!statno_checkterm($term_name,$vacabulary_data['vid'])) {
    form_set_error('statno_term', t('Term does not exist or does not belong to the vocabulary above.'));
  }
}
/**
 * Custom statno function
 *
 * Counts how many times a term happens in nodes
 *
 * @param string $tid
 *   tid
 *
 * @return string $count
 *   count
 */
function statno_countterm($tid) {  
  $query_countterm = db_select('taxonomy_index', 'i');
  $query_countterm ->innerJoin('node', 'n', 'n.nid = i.nid');
  $query_countterm ->condition('n.sticky', '0', '=');  
  $query_countterm->fields('i', array('nid'))->condition('i.tid', $tid, '=');  
  $result_countterm = $query_countterm->countQuery()->execute()->fetchField();
  
  return $result_countterm;
}
/**
 * Custom statno function
 *
 * Shows how many times configured term-vocabulary happens in nodes *
 *
 * @return string $count
 *   count
 */
function statno_showcount() { //incluir a possibildade de optar por não utlizar o sticky 0
  $vid = statno_checkvocabulary(variable_get('statno_vocabulary', 'vocabulary'));
  $tid = statno_checkterm(variable_get('statno_term', 'type'),$vid['vid']);
  $conta = statno_countterm($tid['tid']);
  //drupal_set_message(
  //  'O termo '.variable_get(
  //    'statno_term', 'type'
  //  ).' do vocabulario '.variable_get(
  //    'statno_vocabulary', 'vocabulary'
  //  ).' ocorre em '.$conta.' nodes'
  //);  
  return $conta;
}
/**
 * Custom statno function
 *
 * Gets all terms from configured vocabulary
 *
 * @return array $terms
 *   term name
 */
function statno_termnames() { //incluir a possibildade de optar por não utlizar o sticky 0
  $vid = statno_checkvocabulary(variable_get('statno_vocabulary', 'vocabulary'));
  $query_termnames = db_select('taxonomy_term_data', 't');
  $result_termnames = $query_termnames
    ->fields('t', array('name'))
    ->fields('t', array('tid'))
    ->fields('t', array('vid'))
    ->condition('t.vid', $vid['vid'], '=')
    ->execute()
    ->fetchAll();
  return $result_termnames;  
}

/**
 * Custom statno function
 *
 * Cria string com termos e respectivas estatísticas a partir da array com os termos
 *
 * @return string $estatistics
 *   estatistica dos termos
 */
function statno_estatistica() { //incluir a possibildade de optar por não utlizar o sticky 0
  $termo_result = statno_termnames();
  $termo_backup = variable_get('statno_vocabulary', 'vocabulary');
  //$estatistica = 'Name|stats|tid|vid em '.variable_get('statno_vocabulary', 'vocabulary').':<br>';
  $estatistica = drupal_strtoupper(variable_get('statno_vocabulary', 'vocabulary')).'<br>';
  foreach ($termo_result as $termo){
    variable_set('statno_term', $termo->name);
    //$estatistica = $estatistica.l($termo->name, 'taxonomy/term/'.$termo->tid).'|'.statno_showcount().'|'.$termo->tid.'|'.$termo->vid.'<br>';
    $estatistica = $estatistica.l($termo->name, 'taxonomy/term/'.$termo->tid).': '.statno_showcount().'<br>';
  }
  variable_set('statno_vocabulary', $termo_backup);
  return $estatistica;  
} 

/**
 * Custom statno function
 *
 * Entra com vocabulario definido nas configurações, e pega a quantidade máxima
 * de alternativas - qma. Ou seja, a cardinalidade da categoria com menor
 * quantidade de nodes.
 *
 * @return string $qma
 *   qma
 */
function statno_qma() { //incluir a possibildade de optar por não utlizar o sticky 0
  $vid = statno_checkvocabulary(variable_get('statno_vocabulary', 'vocabulary'));
  
  $query_qma = db_select('statno', 's');
  $result_qma = $query_qma
    ->fields('s', array('statno_stats'))
    ->condition('s.statno_vid', $vid['vid'], '=')
    ->orderBy('statno_stats')
    ->range(0,1)
    ->execute()
    ->fetchAssoc();
  
  $qma = $result_qma['statno_stats'];
  
  //print $qma;
  
  return $qma;  
} 

/**
 * Custom statno function
 *
 * Entra com vocabulario definido nas configurações, e pega a quantidade total
 * de amostras - qta. Ou seja, a soma das statno_stats na tabela statno.
 *
 * @return string $qma
 *   qma
 */
function statno_qta() { //incluir a possibildade de optar por não utlizar o sticky 0
  $vid = statno_checkvocabulary(variable_get('statno_vocabulary', 'vocabulary'));
  
  $query_qta = db_select('statno', 's');
  $result_qta = $query_qta
    ->fields('s', array('statno_stats'))
    ->condition('s.statno_vid', $vid['vid'], '=')    
    ->execute()  
    ->fetchAll();    
    
  //var_dump ($result_qta);
  
  $soma_qta = 0;
  
  foreach ($result_qta as $term){
    //print $term->statno_stats.'|';
    $soma_qta = $term->statno_stats + $soma_qta;
  }
  
  //print $soma_qta;
  
  return $soma_qta;  
} 


/**
 * Implementation of hook_cron().
 *
 * Updates database in regard to statistics
 */
function statno_cron(){  
  //drupal_set_message('Hook cron executado a partir de statno!');
  db_delete('statno')->execute();
  $statno_update = statno_termnames();  
  foreach ($statno_update as $term){
    variable_set('statno_term', $term->name);
    db_insert('statno') 
      ->fields(
        array(
          'statno_tid' => $term->tid,
          'statno_vid' => $term->vid,
          'statno_stats' => statno_countterm($term->tid),
        )
      )
      ->execute();
  }
  return;
}  

//Quantidade máxima de alternativas = a cardinalidade da categoria com menor quantidade de nodes 
//ou
//Quantidade máxima de alternativas = a quantidade de amostras da categoria com a menor quantidade de amostras

//Quantidade mínima de amostras disponíveis para questões positivas = Quantidade máxima de alternativas * 2

//Quantidade máxima de questões = quantidade total de amostras / Quantidade mínima de amostras disponíveis para questões positivas



//para fazer
//
//permitir funcionar com ou sem definição de stick